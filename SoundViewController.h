//
//  SoundViewController.h
//  p03-gupta
//
//  Created by Abhishek Gupta on 2/19/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface SoundViewController : UIViewController <AVAudioPlayerDelegate>
{
    
    IBOutlet UIButton *soundButton;
    IBOutlet UISlider *volumeSlider;
    NSURL *soundFile;
    AVAudioPlayer *sound;
}

-(IBAction)playSound:(id)sender;
- (IBAction)volumeChange;
- (IBAction)resetSound:(id)sender;


@end
