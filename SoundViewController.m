//
//  SoundViewController.m
//  p03-gupta
//
//  Created by Abhishek Gupta on 2/19/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "SoundViewController.h"

@interface SoundViewController ()

@end

@implementation SoundViewController

- (void)viewDidLoad {
    
    soundFile = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"music" ofType:@"mp3"]];
    sound=[[AVAudioPlayer alloc ]initWithContentsOfURL:soundFile error:nil];
    sound.delegate=self;
    sound.volume=0.3;
    
    sound.numberOfLoops=-1;
    [sound play];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)playSound:(id)sender
{
    if(![sound isPlaying])
    {
        [sound play];
        [soundButton setTitle:@"Pause" forState:UIControlStateNormal];
    }
    else{
        [sound pause];
        [soundButton setTitle:@"Play" forState:UIControlStateNormal];
    }
}

- (IBAction)volumeChange {
    sound.volume=volumeSlider.value;
}

- (IBAction)resetSound:(id)sender {
   
    [sound stop];
    sound.currentTime=0;
}


@end
