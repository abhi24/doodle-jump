//
//  ViewController.m
//  p03-gupta
//
//  Created by Abhishek Gupta on 2/15/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>
#define DAMPENING_FACTOR 0.8;
#define DAMPENING_FACTORx 0.4;


@interface ViewController ()
//-(void)myFunc;

@end

@implementation ViewController{
    CMMotionManager *motion;
    NSTimer *timer;
    
}

-(void)myFunc{
    CMAttitude * attitude;
    CMDeviceMotion * motionDevice = motion.deviceMotion;
    attitude = motionDevice.attitude;
    
    if(attitude.roll > 0.01){
        //Go Right;
        BallRight = YES;
    }
    else if(attitude.roll < -0.01){
        //Go left
        BallLeft = YES;
    }
    else{
        //Dont go anywhere.
        BallRight=NO;
        BallLeft = NO;
        StopSideMovement=YES;
        
    }
    
}

- (void)viewDidLoad {
 [super viewDidLoad];
    
    motion =[[CMMotionManager alloc] init];
    motion.deviceMotionUpdateInterval = 1/60;
    [motion startDeviceMotionUpdates ];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:(1/60) target:self selector:@selector(myFunc) userInfo:nil repeats:YES];
    
    
    if([motion isGyroAvailable]){
        if([motion isGyroActive]){
            [motion setGyroUpdateInterval:0.01];
        }
    }
    
//[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(jump) userInfo:NULL repeats:YES];
    Platform1.hidden=YES;
    bridge2.hidden = YES;
    bridge3.hidden = YES;
    bridge4.hidden = YES;
    bridge5.hidden = YES;
    bridge6.hidden = YES;
    bridge7.hidden = YES;
    Home.hidden=YES;
    Reset.hidden=YES;

    gameoverLbl.hidden=YES;
    
   
    
    
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
//{
//    delta.x=acceleration.x *5;
//    Ball.center=CGPointMake(Ball.center.x+delta.x, Ball.center.y+delta.y);
//}


- (IBAction)StartGame:(id)sender
{
//    UIAccelerometer *accel=[UIAccelerometer sharedAccelerometer];
//    accel.delegate=self;
//    accel.updateInterval=1.0f/60.0f;
 
    
    gameoverLbl.hidden=YES;
    textfield.hidden=YES;
    ghost.hidden=YES;
    Start.hidden = YES;
    UpMovement = -5;
    
    Movement = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(shifting) userInfo:nil repeats:YES];
    
    Platform1.hidden=NO;
    bridge2.hidden = NO;
    bridge3.hidden = NO;
    bridge4.hidden = NO;
    bridge5.hidden = NO;
    bridge6.hidden = NO;
    bridge7.hidden = NO;
    
    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 35;
    bridge2.center = CGPointMake(RandomPosition, 475);
    
    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 35;
    bridge3.center = CGPointMake(RandomPosition, 375);
    
    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 100;
    bridge4.center = CGPointMake(RandomPosition, 275);
    
    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 35;
    bridge5.center = CGPointMake(RandomPosition,155);

    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 35;
    bridge6.center = CGPointMake(RandomPosition, 40);
 
    RandomPosition = arc4random()%248;
    RandomPosition = RandomPosition + 35;
    bridge7.center = CGPointMake(RandomPosition, 375);
    
    Platform3SideMovement = -2;
    Platform5SideMovement = 5;
    Platform7SideMovement = 2;
   
}

-(void)GameOver{
    
    gameoverLbl.hidden=NO;
    
    Ball.hidden = YES;
    Platform1.hidden = YES;
    bridge2.hidden = YES;
    bridge3.hidden = YES;
    bridge4.hidden = YES;
    bridge5.hidden = YES;
    bridge6.hidden = YES;
    bridge7.hidden = YES;
    ScoreValue.hidden=NO;
    ScoreValue.text = [NSString stringWithFormat:@"%d",ScoreNumber];
    NSLog(@"VALUE IS : %d", ScoreNumber);
    i=0;
    Home.hidden=NO;
    Reset.hidden=NO;
    [Movement invalidate];
    

}

int i=0;
int  ScoreNumber = 0;

-(void)score{

    ScoreNumber = ScoreNumber + 1;

   ScoreValue.text = [NSString stringWithFormat:@"%d",ScoreNumber];
    NSLog(@"%d", ScoreNumber);

}
-(void)shifting
{
    
    
    if (Ball.center.y > 680 && i==0)
    {
        Ball.center = CGPointMake(Ball.center.x, 580);
        i++;
    }
    if (Ball.center.y > 670 && i)
    {
        [self GameOver];
    }
    
    
    [self bridgemove];
    
    Ball.center = CGPointMake(Ball.center.x + SideMovement, Ball.center.y - UpMovement);


    if ((CGRectIntersectsRect(Ball.frame, bridge2.frame)))
    {
        [self Bounce];
         bridgefall = 3;
        [self score];
    }
    if ((CGRectIntersectsRect(Ball.frame, Platform1.frame)) && (UpMovement < -2))
    {
        [self Bounce];
         bridgefall = 3;
        [self score];
    }
    
    if ((CGRectIntersectsRect(Ball.frame, bridge3.frame)) && (UpMovement < -2))
    {
        [self GameOver];
    }
    
    if ((CGRectIntersectsRect(Ball.frame, bridge4.frame)) && (UpMovement < -2))
    {
        [self Bounce];
        bridgefall = 3;
        [self score];
    }
    
    if ((CGRectIntersectsRect(Ball.frame, bridge5.frame)) && (UpMovement < -2))
    {
        [self Bounce];
        bridgefall = 6;
        [self score];
    }
    
    if ((CGRectIntersectsRect(Ball.frame, bridge6.frame)) && (UpMovement < -2))
    {
        [self Bounce];
        bridgefall = 3;
        [self score];
    }
    
    if ((CGRectIntersectsRect(Ball.frame, bridge7.frame)) && (UpMovement < -2))
    {
        [self Bounce];
        bridgefall = 3;
        [self score];
    }
    
    
    
    
    if (BallLeft == YES)
    {
        SideMovement = SideMovement - 0.3;
        if (SideMovement < -5)
        {
            SideMovement = -5;
        }
    }
    
    if (BallRight == YES)
    {
        SideMovement = SideMovement + 0.3;
        if(SideMovement > 5)
        {
            SideMovement = 5;
        }
    }
    
    if (StopSideMovement == YES && SideMovement > 0)
    {
        SideMovement = SideMovement - 0.1;
        if(SideMovement < 0)
        {
            SideMovement = 0;
            StopSideMovement = NO;
        }
    }
    
    if (StopSideMovement == YES && SideMovement < 0)
    {
        SideMovement = SideMovement + 0.1;
        if(SideMovement > 0)
        {
            SideMovement = 0;
            StopSideMovement = NO;
        }
    }
    

    
    
    UpMovement = UpMovement - 0.5;

    if (Ball.center.x < -11)
    {
        Ball.center = CGPointMake(330, Ball.center.y);
    }

    if (Ball.center.x > 330)
    {
        Ball.center = CGPointMake(-11, Ball.center.y);
    }
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.view];
    
    if (point.x < 160)
    {
        BallLeft = YES;
    }
    else
    {
        BallRight = YES;
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    BallLeft = NO;
    BallRight = NO;
    StopSideMovement = YES;
    
}

-(void)bridgemove
{
    Platform1.center = CGPointMake(Platform1.center.x, Platform1.center.y + bridgefall);
    bridge2.center = CGPointMake(bridge2.center.x, bridge2.center.y + bridgefall);
    bridge3.center = CGPointMake(bridge3.center.x + Platform3SideMovement, bridge3.center.y + 1.5);
    bridge4.center = CGPointMake(bridge4.center.x, bridge4.center.y + bridgefall);
    bridge5.center = CGPointMake(bridge5.center.x + Platform5SideMovement, bridge5.center.y + bridgefall);
    bridge6.center = CGPointMake(bridge6.center.x, bridge6.center.y + bridgefall);
    bridge7.center = CGPointMake(bridge7.center.x + Platform7SideMovement, bridge7.center.y + bridgefall);
    
    if (bridge3.center.x < 50)
    {
        Platform3SideMovement = 6;
    }
    if (bridge3.center.x > 150)
    {
        Platform3SideMovement = -4;
    }
  
    if (bridge5.center.x < 50)
    {
        Platform5SideMovement = 6;
    }
    if (bridge5.center.x > 250)
    {
        Platform5SideMovement = -4;
    }
   
    if (bridge7.center.x < 50)
    {
        Platform7SideMovement = 6;
    }
    if (bridge7.center.x > 300)
    {
        Platform7SideMovement = -6;
    }
    
    bridgefall = bridgefall - 0.1;
    
    if (bridgefall < 0)
    {
        bridgefall = 0;
    }
  
    if (Platform1.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        Platform1.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge2.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge2.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge3.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge3.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge4.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge4.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge5.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge5.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge6.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge6.center = CGPointMake(RandomPosition, -6);
    }
    if (bridge7.center.y > 670)
    {
        RandomPosition = arc4random()%246;
        RandomPosition = RandomPosition + 35;
        bridge7.center = CGPointMake(RandomPosition, -6);
    }
    
    
}


-(void)Bounce{
    
    Ball.animationImages = [NSArray arrayWithObjects:
                            [UIImage imageNamed:@"1.png"],
                            [UIImage imageNamed:@"2.png"],[UIImage imageNamed:@"3.png"],nil];
    
    [Ball setAnimationRepeatCount:1];
    Ball.animationDuration = 1.0;
    [Ball startAnimating];
    
        UpMovement = 9.5;
    
}
@end
