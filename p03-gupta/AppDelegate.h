//
//  AppDelegate.h
//  p03-gupta
//
//  Created by Abhishek Gupta on 2/15/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

